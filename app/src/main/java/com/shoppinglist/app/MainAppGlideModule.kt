package com.shoppinglist.app

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MainAppGlideModule : AppGlideModule()