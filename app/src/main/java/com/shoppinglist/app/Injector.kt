package com.shoppinglist.app

import android.content.Context
import com.shoppinglist.repository.database.ProductsDao
import com.shoppinglist.repository.database.ProductsDatabase
import com.shoppinglist.ui.MainViewModelFactory
import com.shoppinglist.ui.main.edit.EditViewModelFactory
import com.shoppinglist.ui.main.home.HomeViewModelFactory
import com.shoppinglist.ui.main.home.add_goods.AddGoodsViewModelFactory
import com.shoppinglist.ui.main.preview.PreviewViewModelFactory
import com.shoppinglist.ui.register.RegisterViewModelFactory
import com.shoppinglist.ui.splash.SplashViewModelFactory

object Injector {
    fun provideMainViewModelFactory() =
        MainViewModelFactory()

    fun provideSplashViewModelFactory() =
        SplashViewModelFactory()

    fun provideHomeViewModelFactory() =
        HomeViewModelFactory()

    fun provideRegisterViewModelFactory() =
        RegisterViewModelFactory()

    fun provideEditViewModelFactory() =
        EditViewModelFactory()

    fun provideAddGoodsViewModelFactory() =
        AddGoodsViewModelFactory()

    fun providePreviewViewModelFactory() =
        PreviewViewModelFactory()

    fun provideProductsDatabase(context: Context): ProductsDatabase =
        ProductsDatabase.getInstance(context.applicationContext)

    fun provideProductsDatabaseDao(context: Context): ProductsDao =
        provideProductsDatabase(context.applicationContext).productsDao()
}