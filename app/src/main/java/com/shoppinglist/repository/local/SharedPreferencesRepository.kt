package com.shoppinglist.repository.local

import android.content.Context
import android.content.SharedPreferences
import com.shoppinglist.app.App

private const val SETTINGS_FILE = "com.shoppingList.settingsFile"
private const val PREF_KEY_USER_EMAIL = "PREF_KEY_USER_EMAIL"

object SharedPreferencesRepository {
    private fun getSharedPreferences(): SharedPreferences {
        return App.applicationContext.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
    }

    fun getEmail(): String {
        return getSharedPreferences().getString(PREF_KEY_USER_EMAIL, "") ?: ""
    }

    fun saveEmail(email: String) {
        with(getSharedPreferences().edit()) {
            putString(PREF_KEY_USER_EMAIL, email)
            apply()
        }
    }

    fun clear() {
        with(getSharedPreferences().edit()) {
            clear()
            apply()
        }
    }
}