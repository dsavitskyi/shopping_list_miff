package com.shoppinglist.repository.errors

import com.shoppinglist.R
import com.shoppinglist.app.App
import java.io.IOException

class NoNetworkException : IOException(App.applicationContext.getString(R.string.error_no_network))