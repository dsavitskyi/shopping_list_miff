package com.shoppinglist.repository.models

import com.google.gson.annotations.SerializedName

data class BaseResponse<T> (
    @SerializedName("success") val success: Boolean,
    @SerializedName("errors_message") val errorsMessage: String?,
    @SerializedName("data") val data: T?
)