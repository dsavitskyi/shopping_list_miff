package com.shoppinglist.ui.splash

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseFragment
import com.shoppinglist.repository.local.SharedPreferencesRepository
import com.shoppinglist.utils.Const.SPLASH_SCREEN_DELAY

class SplashFragment : BaseFragment<SplashViewModel, SplashFragment.Events>() {

    override val viewModel: SplashViewModel by viewModels {
        Injector.provideSplashViewModelFactory()
    }

    override val layout by lazy { R.layout.splash_fragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.decorView?.apply {
            // Hide both the navigation bar and the status bar.
            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
            // a general rule, you should design your app to hide the status bar whenever you
            // hide the navigation bar.
            systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            val attrib = activity?.window?.attributes
            attrib?.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }
        Handler().postDelayed({
            activity?.window?.decorView?.apply {
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_VISIBLE
            }
            val userEmail= SharedPreferencesRepository.getEmail()
            if (userEmail.isNotBlank()) {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
            } else {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToRegisterFragment())
            }
        }, SPLASH_SCREEN_DELAY)
    }

    enum class Events {}
}