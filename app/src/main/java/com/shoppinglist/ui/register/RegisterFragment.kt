package com.shoppinglist.ui.register

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseFragment
import com.shoppinglist.architecture.extensions.hasNetworkConnection
import com.shoppinglist.architecture.extensions.toast
import com.shoppinglist.repository.errors.NoNetworkException
import com.shoppinglist.repository.local.SharedPreferencesRepository
import com.shoppinglist.utils.TextUtils.isValidEmail
import kotlinx.android.synthetic.main.register_fragment.*

class RegisterFragment : BaseFragment<RegisterViewModel, RegisterFragment.Events>() {

    private lateinit var auth: FirebaseAuth

    override val viewModel: RegisterViewModel by viewModels {
        Injector.provideRegisterViewModelFactory()
    }

    override val layout by lazy { R.layout.register_fragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    private fun bindUI() {
        signup_btn.setOnClickListener {
            val email: String = email_edt_text.text.toString()
            val password: String = pass_edt_text.text.toString()

            if (requireContext().hasNetworkConnection()) {
                when {
                    TextUtils.isEmpty(email) || TextUtils.isEmpty(password) -> {
                        toast(getString(R.string.txt_fill_all_the_fields))
                    }
                    isValidEmail(email).not() -> {
                        toast(getString(R.string.txt_valid_email))
                    }
                    password.length < 8 -> {
                        toast(getString(R.string.txt_min_pass_length))
                    }
                    else -> {
                        auth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToHomeFragment())
                                    SharedPreferencesRepository.saveEmail(email)
                                } else {
                                    toast(getString(R.string.txt_registration_failed))
                                }
                            }
                    }
                }
            } else {
                toast(getString(R.string.txt_no_network_connection))
            }
        }

        login_btn.setOnClickListener {
            val email: String = email_edt_text.text.toString()
            val password: String = pass_edt_text.text.toString()

            if (requireContext().hasNetworkConnection()) {
                when {
                    TextUtils.isEmpty(email) || TextUtils.isEmpty(password) -> {
                        toast(getString(R.string.txt_fill_all_the_fields))
                    }
                    isValidEmail(email).not() -> {
                        toast(getString(R.string.txt_valid_email))
                    }
                    password.length < 8 -> {
                        toast(getString(R.string.txt_min_pass_length))
                    }
                    else -> {
                        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToHomeFragment())
                                SharedPreferencesRepository.saveEmail(email)
                            } else {
                                toast(getString(R.string.txt_login_faled))
                            }
                        }
                    }
                }
            } else {
                toast(getString(R.string.txt_no_network_connection))
            }
        }
    }

    enum class Events {}
}