package com.shoppinglist.ui

import android.os.Bundle
import androidx.activity.viewModels
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseActivity
import com.shoppinglist.architecture.extensions.gone
import com.shoppinglist.architecture.extensions.visible
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : BaseActivity<MainViewModel, MainActivity.Events>() {

    override val viewModel: MainViewModel by viewModels {
        Injector.provideMainViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

    override fun showLoading(show: Boolean) {
        if (show) {
            pb_loading?.visible()
        } else {
            pb_loading?.gone()
        }
    }

    enum class Events {}
}
