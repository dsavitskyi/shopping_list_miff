package com.shoppinglist.ui.main.home.add_goods

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AddGoodsViewModelFactory() : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = AddGoodsViewModel() as T
}
