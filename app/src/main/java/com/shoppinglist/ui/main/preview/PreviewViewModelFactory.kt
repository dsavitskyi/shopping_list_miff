package com.shoppinglist.ui.main.preview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PreviewViewModelFactory() : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = PreviewViewModel() as T
}
