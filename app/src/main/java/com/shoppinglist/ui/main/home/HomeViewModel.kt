package com.shoppinglist.ui.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.shoppinglist.architecture.base.BaseViewModel
import com.shoppinglist.architecture.internal.events.Event
import com.shoppinglist.repository.database.ProductsDao
import com.shoppinglist.repository.local.SharedPreferencesRepository
import com.shoppinglist.repository.models.ShoppingModel
import com.shoppinglist.repository.models.ShoppingModelFirebase
import com.shoppinglist.utils.Const

class HomeViewModel : BaseViewModel<HomeFragment.Events>() {

    lateinit var productsDao: ProductsDao
    var productsLiveData: LiveData<List<ShoppingModel>> = MutableLiveData()
    private val userEmail = SharedPreferencesRepository.getEmail()
    private var products = ArrayList<ShoppingModel>()
    var isProductItemDeleted = false

    val productsDb = FirebaseDatabase.getInstance(Const.FIREBASE_DB_URL)
    val productsRef = productsDb.reference.child(Const.TABLE_NAME).child(userEmail.replace(".",""))

    fun initDatabase(productsDao: ProductsDao) {
        this.productsDao = productsDao
    }

    fun initFBDatabase() {
        productsRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                products.clear()
                for (postSnapshot in snapshot.children) {
                    val shopping: ShoppingModelFirebase = postSnapshot.getValue(
                        ShoppingModelFirebase::class.java)!!
                    products.add(shopping.toShoppingRoomModel(true))
                }
                insertProducts(products)
            }
        })
    }

    fun getProducts() {
        showLoading()
        productsLiveData = productsDao.getProducts()
        hideLoading()
    }

    fun insertProducts(products: List<ShoppingModel>) {
        launchCoroutine({
            productsDao.insertProducts(products)
        }, {
            Log.d("Shopping", "Error delete product")
        })
    }

    fun deleteProduct(product: ShoppingModel) {
        showLoading()
        launchCoroutine({
            isProductItemDeleted = true
            productsDao.deleteProduct(product)
            hideLoading()
        }, {
            hideLoading()
            isProductItemDeleted = false
            Log.d("Shopping", "Error delete product")
        })
    }

    fun cleanDatabase() {
        showLoading()
        launchCoroutine({
            productsDao.cleanDataBase()
            hideLoading()
        }, {
            hideLoading()
            Log.d("Shopping", "Error clean database")
        })
    }
}