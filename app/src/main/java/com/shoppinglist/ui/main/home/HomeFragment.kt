package com.shoppinglist.ui.main.home

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseFragment
import com.shoppinglist.architecture.extensions.invisible
import com.shoppinglist.architecture.extensions.visible
import com.shoppinglist.architecture.internal.setMarginBottom
import com.shoppinglist.architecture.internal.setMarginTop
import com.shoppinglist.repository.local.SharedPreferencesRepository
import com.shoppinglist.repository.models.ShoppingModel
import com.shoppinglist.ui.main.home.adapter.ProductsPagedAdapter
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment : BaseFragment<HomeViewModel, HomeFragment.Events>() {

    private val productsPagedAdapter = ProductsPagedAdapter()

    override val viewModel: HomeViewModel by viewModels {
        Injector.provideHomeViewModelFactory()
    }

    override fun getStatusBarIconColor(): Boolean = false

    override val layout by lazy { R.layout.home_fragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productsPagedAdapter.productsListener = productsListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(fl_content) { v, insets ->
            cl_work_area?.setMarginTop(insets.systemWindowInsetTop)
            cl_content?.setMarginBottom(insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
        viewModel.initFBDatabase()
        setupToolbar()
        bindUI()
        viewModel.initDatabase(Injector.provideProductsDatabaseDao(view.context))
        viewModel.getProducts()

        viewModel.productsLiveData.observe(viewLifecycleOwner, Observer { products ->
            productsPagedAdapter.submitList(products)
            val checkForSynck = products.filter { !it.syncked }
            if (checkForSynck.isNotEmpty() || viewModel.isProductItemDeleted) {
                viewModel.productsRef.setValue(products.map { it.toShoppingModelFifeBase() })
                viewModel.isProductItemDeleted = false
            }
            if (products.isEmpty())
                tv_add_product.visible()
            else
                tv_add_product.invisible()
        })
    }

    private fun bindUI() {
        with(rv_shopping_items) {
            layoutManager = LinearLayoutManager(context)
            adapter = productsPagedAdapter
            itemAnimator = DefaultItemAnimator()
        }
        fb_add_good?.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAddGoodsDialogFragment())
        }
        ic_log_out?.setOnClickListener {
            viewModel.cleanDatabase()
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToRegisterFragment())
            SharedPreferencesRepository.clear()
        }
    }

    private val productsListener = object : ProductsPagedAdapter.ProductsListListener {
        override fun onEditClick(modelPart: ShoppingModel) {
            findNavController().navigate(HomeFragmentDirections
                .actionHomeFragmentToEditFragment(product = modelPart))
        }

        override fun onDeleteClick(modelPart: ShoppingModel) {
            viewModel.deleteProduct(modelPart)
        }

        override fun onPreview(modelPart: ShoppingModel) {
            findNavController().navigate(HomeFragmentDirections
                .actionHomeFragmentToPreviewFragment(product = modelPart))
        }
    }

    enum class Events {}
}