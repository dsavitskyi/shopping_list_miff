package com.shoppinglist.ui.main.home.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.shoppinglist.architecture.base.BaseListAdapter
import com.shoppinglist.architecture.base.BaseViewHolder
import com.shoppinglist.repository.models.ShoppingModel

class ProductsPagedAdapter : BaseListAdapter<ShoppingModel>(DIFF_CALLBACK) {
    lateinit var productsListener: ProductsListListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ProductsViewHolder(parent, productsListener)

    override fun onBindViewHolder(
        holder: BaseViewHolder<ShoppingModel>,
        position: Int, payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
    }

    interface ProductsListListener {
        fun onEditClick(modelPart: ShoppingModel)
        fun onDeleteClick(modelPart: ShoppingModel)
        fun onPreview(modelPart: ShoppingModel)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ShoppingModel>() {
            override fun areItemsTheSame(oldModel: ShoppingModel, newModel: ShoppingModel) =
                oldModel.id == newModel.id

            override fun areContentsTheSame(oldModel: ShoppingModel, newModel: ShoppingModel) =
                oldModel == newModel
        }
    }
}