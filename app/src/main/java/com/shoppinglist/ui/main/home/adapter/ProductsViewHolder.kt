package com.shoppinglist.ui.main.home.adapter

import android.view.ViewGroup
import com.shoppinglist.R
import com.shoppinglist.architecture.base.BaseViewHolder
import com.shoppinglist.architecture.internal.setThrottleOnClickListener
import com.shoppinglist.architecture.internal.threadUnsafeLazy
import com.shoppinglist.repository.models.ShoppingModel
import kotlinx.android.synthetic.main.item_shopping.view.*

class ProductsViewHolder(
    parent: ViewGroup,
    private val listener: ProductsPagedAdapter.ProductsListListener
) : BaseViewHolder<ShoppingModel>(parent, R.layout.item_shopping, null) {

    private val tvProductTitle by threadUnsafeLazy { itemView.tv_product_name }
    private val tvProductDate by threadUnsafeLazy { itemView.tv_product_date }
    private val ivEdit by threadUnsafeLazy { itemView.iv_edit}
    private val ivDelete by threadUnsafeLazy { itemView.iv_delete}
    private val cvPreview by threadUnsafeLazy { itemView.cv_item_container}


    override fun onBind(item: ShoppingModel) {
        super.onBind(item)

        tvProductTitle.text = item.title
        tvProductDate.text = item.publishedAt

        ivEdit.setThrottleOnClickListener {
            listener.onEditClick(item)
        }

        ivDelete.setThrottleOnClickListener {
            listener.onDeleteClick(item)
        }

        cvPreview.setThrottleOnClickListener {
            listener.onPreview(item)
        }
    }
}