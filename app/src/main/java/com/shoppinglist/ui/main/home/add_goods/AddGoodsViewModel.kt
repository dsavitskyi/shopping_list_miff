package com.shoppinglist.ui.main.home.add_goods

import android.util.Log
import com.shoppinglist.architecture.base.BaseViewModel
import com.shoppinglist.architecture.internal.events.Event
import com.shoppinglist.repository.database.ProductsDao
import com.shoppinglist.repository.models.ShoppingModel

class AddGoodsViewModel : BaseViewModel<AddGoodsDialogFragment.Events>() {

    lateinit var productsDao: ProductsDao

    fun initDatabase(productsDao: ProductsDao) {
        this.productsDao = productsDao
    }

    fun insertProduct(shopping: ShoppingModel) {
        launchCoroutine({
            productsDao.insertProduct(shopping)
            _eventsLiveData.value = Event(AddGoodsDialogFragment.Events.INSERT_SUCCESSFUL)
        }, {
            Log.d("Shopping", "Error adding product")
        })
    }
}