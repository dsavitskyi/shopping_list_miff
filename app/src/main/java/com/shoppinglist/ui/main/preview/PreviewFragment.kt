package com.shoppinglist.ui.main.preview

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseFragment
import com.shoppinglist.architecture.internal.setMarginBottom
import com.shoppinglist.architecture.internal.setMarginTop
import kotlinx.android.synthetic.main.preview_fragment.*

class PreviewFragment : BaseFragment<PreviewViewModel, PreviewFragment.Events>() {

    val args: PreviewFragmentArgs by navArgs()

    override val viewModel: PreviewViewModel by viewModels {
        Injector.providePreviewViewModelFactory()
    }

    override val layout by lazy { R.layout.preview_fragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(fl_content) { v, insets ->
            cl_work_area?.setMarginTop(insets.systemWindowInsetTop)
            cl_content?.setMarginBottom(insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
        setupToolbar()
        bindUI()
    }

    private fun bindUI() {
        et_edit_product.text = args.product.title
    }

    enum class Events {}
}