package com.shoppinglist.ui.main.edit

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseFragment
import com.shoppinglist.architecture.extensions.toast
import com.shoppinglist.architecture.internal.setMarginBottom
import com.shoppinglist.architecture.internal.setMarginTop
import com.shoppinglist.repository.models.ShoppingModel
import com.shoppinglist.utils.TextUtils
import kotlinx.android.synthetic.main.edit_fragment.*
import java.text.DateFormat
import java.util.*

class EditFragment : BaseFragment<EditViewModel, EditFragment.Events>() {

    val args: EditFragmentArgs by navArgs()

    override val viewModel: EditViewModel by viewModels {
        Injector.provideEditViewModelFactory()
    }

    override val layout by lazy { R.layout.edit_fragment }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when(event) {
            Events.SAVED_SUCCESSFUL -> toast(getString(R.string.txt_changes_saved))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(fl_content) { v, insets ->
            cl_work_area?.setMarginTop(insets.systemWindowInsetTop)
            cl_content?.setMarginBottom(insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
        viewModel.initDatabase(Injector.provideProductsDatabaseDao(view.context))
        setupToolbar()
        bindUI()
    }

    private fun bindUI() {
        et_edit_product.setText(args.product.title, TextView.BufferType.EDITABLE)
        val currentDate = TextUtils.formatDate()
        tv_save_product?.setOnClickListener {
            val product = ShoppingModel(id = args.product.id, title = et_edit_product.text.toString(),
                publishedAt = currentDate)
            viewModel.editProduct(product) }
    }

    enum class Events {
        SAVED_SUCCESSFUL
    }
}