package com.shoppinglist.ui.main.home.add_goods

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.viewModels
import com.shoppinglist.R
import com.shoppinglist.app.Injector
import com.shoppinglist.architecture.base.BaseDialogFragment
import com.shoppinglist.architecture.extensions.toast
import com.shoppinglist.architecture.internal.toPx
import com.shoppinglist.repository.models.ShoppingModel
import com.shoppinglist.utils.TextUtils
import kotlinx.android.synthetic.main.add_good_dialog_fragment.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AddGoodsDialogFragment :
    BaseDialogFragment<AddGoodsViewModel, AddGoodsDialogFragment.Events>() {

    override val viewModel: AddGoodsViewModel by viewModels {
        Injector.provideAddGoodsViewModelFactory()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return Dialog(requireContext(), theme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.add_good_dialog_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initDatabase(Injector.provideProductsDatabaseDao(view.context))
        bindUI()
    }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when (event) {
            Events.INSERT_SUCCESSFUL -> dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (dialog != null && dialog?.window != null) {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                8.toPx(),
                0,
                8.toPx(),
                0
            )
            dialog?.window?.setBackgroundDrawable(inset)
        }
    }

    private fun bindUI() {
        tv_add_to_list.setOnClickListener {
            if (et_add_goods.text.toString().isNotEmpty()) {
                val currentDate = TextUtils.formatDate()
                val product = ShoppingModel(title = et_add_goods.text.toString(), publishedAt = currentDate)
                viewModel.insertProduct(product)
            } else {
                toast(getString(R.string.txt_add_product_description))
            }
        }
    }

    enum class Events {
        INSERT_SUCCESSFUL
    }
}