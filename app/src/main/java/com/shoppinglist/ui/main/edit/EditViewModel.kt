package com.shoppinglist.ui.main.edit

import android.util.Log
import com.shoppinglist.architecture.base.BaseViewModel
import com.shoppinglist.architecture.internal.events.Event
import com.shoppinglist.repository.database.ProductsDao
import com.shoppinglist.repository.models.ShoppingModel
import com.shoppinglist.ui.main.home.add_goods.AddGoodsDialogFragment

class EditViewModel : BaseViewModel<EditFragment.Events>() {

    lateinit var productsDao: ProductsDao

    fun initDatabase(productsDao: ProductsDao) {
        this.productsDao = productsDao
    }

    fun editProduct(shopping: ShoppingModel) {
        launchCoroutine({
            productsDao.updateProduct(shopping)
            _eventsLiveData.value = Event(EditFragment.Events.SAVED_SUCCESSFUL)
        }, {
            Log.d("Shopping", "Error adding product")
        })
    }
}