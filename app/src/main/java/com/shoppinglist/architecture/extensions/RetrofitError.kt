package com.shoppinglist.architecture.extensions

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import retrofit2.Response

inline fun <reified T> Response<*>.parseErrorJsonResponse(): T?
{
    val response = errorBody()?.string()
    if(response != null)
        try {
            return Gson().fromJson(response)
        } catch(e: JsonSyntaxException) {
            e.printStackTrace()
        }
    return null
}