package com.shoppinglist.architecture.internal

import android.text.Editable
import android.view.View
import android.widget.EditText

private const val CLICK_DELAY_MILLIS = 500L

fun View.setThrottleOnClickListener(callback: (view: View) -> Unit) {
    var lastClickTime = 0L

    this.setOnClickListener {
        val currentTimeMillis = System.currentTimeMillis()

        if (currentTimeMillis - lastClickTime > CLICK_DELAY_MILLIS) {
            lastClickTime = currentTimeMillis
            callback.invoke(it)
        }
    }
}

fun EditText.afterTextChanged(callback: (String) -> Unit) =
    this.addTextChangedListener(object : SimpleTextWatcher() {
        override fun afterTextChanged(text: Editable) {
            super.afterTextChanged(text)
            callback(text.toString())
        }
    })
