package com.shoppinglist.architecture.internal

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.PluralsRes
import androidx.core.content.ContextCompat
import kotlin.math.roundToInt

fun Int.toPx() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), Resources.getSystem().displayMetrics).toInt()

fun Float.toPx() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics).toInt()

fun Boolean?.falseIfNull() = this ?: false

fun Long.secondsToMillis() = this * 1000L

fun String.toNumericString() = this.filter { it.isDigit() }

fun String?.nullIfBlank() = if (this.isNullOrBlank()) null else this

fun <T> threadUnsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)

fun <E> Iterable<E>.replace(old: E, new: E) = map { if (it == old) new else it }

fun Context.toColor(@ColorRes resId: Int) = ContextCompat.getColor(this, resId)

@Suppress("UnsafeCallOnNullableType")
fun Context.toDrawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(this, resId)!!

@Suppress("RemoveRedundantQualifierName")
fun Context.toColorWithAlpha(@ColorRes resId: Int, ratio: Float) =
    ContextCompat.getColor(this, resId).addAlpha(ratio)

fun Activity.hideKeyboard() {
    this.currentFocus?.let {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}

fun EditText.showKeyboard() {
    requestFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun EditText.hideKeyboard() {
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(this.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

fun Context.getPlural(@PluralsRes pluralRes: Int, count: Int): String =
    this.resources.getQuantityString(pluralRes, count, count)

fun Int.addAlpha(ratio: Float): Int {
    val newColor: Int
    val alpha = (Color.alpha(this) * ratio).roundToInt()
    val r = Color.red(this)
    val g = Color.green(this)
    val b = Color.blue(this)
    newColor = Color.argb(alpha, r, g, b)
    return newColor
}

fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}

fun View.setMarginTop(marginTop: Int) {
    val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
    menuLayoutParams.setMargins(0, marginTop, 0, 0)
    this.layoutParams = menuLayoutParams
}

fun View.setMarginBottom(marginBottom: Int) {
    val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
    menuLayoutParams.setMargins(0, 0, 0, marginBottom)
    this.layoutParams = menuLayoutParams
}