package com.shoppinglist.architecture.base

interface OnBackPressListener {
    fun onBackPressed(): Boolean
}