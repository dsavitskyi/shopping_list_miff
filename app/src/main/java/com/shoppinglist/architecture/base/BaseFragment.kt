package com.shoppinglist.architecture.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.shoppinglist.R
import com.shoppinglist.architecture.extensions.toast
import com.shoppinglist.architecture.internal.events.EventObserver
import com.shoppinglist.architecture.internal.hideKeyboard
import com.jaeger.library.StatusBarUtil

abstract class BaseFragment<V: BaseViewModel<E>, E> : Fragment() {
    protected abstract val viewModel: V

    protected abstract val layout: Int

    protected var toolbarDelegate: ToolbarDelegate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribeToEvents()
        subscribeToToast()
        subscribeProgressEvents()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resolveStatusBarAndIconsColor()
    }

    private fun subscribeToEvents() {
        viewModel.eventsLiveData.observe(this, EventObserver {
            it?.let { onEvent(it) }
        })
        viewModel.logout.observe(this, Observer {
            if (it == true) {
                logout()
            }
        })
    }

    protected open fun logout() {
        //findNavController().navigate(NavGraphDirections.actionGlobalEnterphoneFragment())
    }

    protected open fun onEvent(event: E) {

    }

    override fun onStop() {
        activity?.hideKeyboard()
        super.onStop()
    }

    protected fun hideKeyboard() {
        activity?.hideKeyboard()
    }

    protected open fun setupToolbar(@MenuRes menuRes: Int? = null, navigateUp: Boolean = true) {
        toolbarDelegate = ToolbarDelegate(this)
        if (navigateUp) {
            toolbarDelegate?.setupToolbar()
        }
        if (menuRes != null) {
            toolbarDelegate?.inflateMenu(menuRes)
        }
    }

    private fun subscribeToToast() {
        observe(viewModel.toastMessage) {
            when (it) {
                is String -> toast(it, Toast.LENGTH_SHORT)
                is Int -> toast(it, Toast.LENGTH_SHORT)
            }
        }
    }

    protected open fun showLoading(show: Boolean) {
        (activity as? BaseActivity<*, *>)?.showLoading(show)
    }

    private fun subscribeProgressEvents() {
        observe(viewModel.progressVisibility) {
            showLoading(it)
        }
    }

    open fun setToolbarTitle(title: String) {
        toolbarDelegate?.setToolbarTitle(title)
    }

    open fun setToolbarTitle(@StringRes resId: Int) {
        toolbarDelegate?.setToolbarTitle(resId)
    }

    fun <T, LD : LiveData<T>> observeNullable(liveData: LD, onChanged: (T?) -> Unit) {
        liveData.observe(this, Observer {
            onChanged(it)
        })
    }

    fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer {
            it?.let(onChanged)
        })
    }

    override fun onDestroyView() {
        toolbarDelegate = null
        showLoading(false)
        super.onDestroyView()
    }

    open fun getStatusBarIconColor(): Boolean {
        return true //If true icons will be white otherwise status bar icons will be black
    }



    private fun resolveStatusBarAndIconsColor() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            if (getStatusBarIconColor()) {
                activity?.window?.statusBarColor = Color.TRANSPARENT
            } else {
                activity?.window?.statusBarColor = ContextCompat.getColor(requireContext(), R.color.black_transparent)
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getStatusBarIconColor()) {
                StatusBarUtil.setDarkMode(activity)
            } else {
                StatusBarUtil.setLightMode(activity)
            }
        }
    }

    // open fun onBackPressed() = true
}