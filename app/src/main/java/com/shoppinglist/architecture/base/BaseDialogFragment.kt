package com.shoppinglist.architecture.base

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.shoppinglist.architecture.extensions.toast
import com.shoppinglist.architecture.internal.events.EventObserver
import com.shoppinglist.architecture.internal.hideKeyboard

abstract class BaseDialogFragment<V: BaseViewModel<E>, E> : DialogFragment() {
    protected abstract val viewModel: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribeToEvents()
        subscribeToToast()
        subscribeProgressEvents()
    }

    private fun subscribeToEvents() {
        viewModel.eventsLiveData.observe(this, EventObserver {
            it?.let { onEvent(it) }
        })
        viewModel.logout.observe(this, Observer {
            if (it == true) {
                logout()
            }
        })
    }

    protected open fun logout() {

    }

    protected open fun onEvent(event: E) {

    }

    override fun onStop() {
        activity?.hideKeyboard()
        super.onStop()
    }

    protected fun hideKeyboard() {
        activity?.hideKeyboard()
    }

    private fun subscribeToToast() {
        observe(viewModel.toastMessage) {
            when (it) {
                is String -> toast(it, Toast.LENGTH_SHORT)
                is Int -> toast(it, Toast.LENGTH_SHORT)
            }
        }
    }

    protected open fun showLoading(show: Boolean) {
        (activity as? BaseActivity<*, *>)?.showLoading(show)
    }

    private fun subscribeProgressEvents() {
        observe(viewModel.progressVisibility) {
            showLoading(it)
        }
    }

    fun <T, LD : LiveData<T>> observeNullable(liveData: LD, onChanged: (T?) -> Unit) {
        liveData.observe(this, Observer {
            onChanged(it)
        })
    }

    fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer {
            it?.let(onChanged)
        })
    }
}