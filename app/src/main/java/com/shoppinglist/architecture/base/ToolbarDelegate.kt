package com.shoppinglist.architecture.base

import android.graphics.Color
import android.util.Log
import android.view.Menu
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.shoppinglist.R
import com.shoppinglist.architecture.internal.toColor

class ToolbarDelegate(private val fragment: Fragment) {

    private var toolbar: Toolbar? = fragment.view?.findViewById(R.id.toolbar) as? Toolbar

    var toolbarNavigateUpListener: ToolbarNavigateUpListener? = null

    fun setupToolbar() {
        toolbar?.setNavigationOnClickListener {
            navigateUp()
        }
    }

    fun navigateUp() {
        val onNavigateUp = toolbarNavigateUpListener?.onNavigateUp() ?: false
        if (!onNavigateUp) {
            val handled = getNavController()?.navigateUp() ?: false
            if (!handled) {
                fragment.activity?.onBackPressed()
            }
        }
    }

    private fun getNavController(): NavController? {
        return try {
            fragment.view?.let { Navigation.findNavController(it) }
        } catch (e: Throwable) {
            Log.e("Error", e.message.orEmpty())
            null
        }
    }

    fun setToolbarTitle(@StringRes titleRes: Int) {
        toolbar?.setTitle(titleRes)
        toolbar?.subtitle = null
    }

    fun setToolbarSubTitle(@StringRes subTitleRes: Int) {
        toolbar?.setSubtitle(subTitleRes)
    }

    fun setToolbarSubTitle(subTitle: String) {
        toolbar?.subtitle = subTitle
    }

    fun setToolbarBackground(@ColorRes colorResource: Int) {
        val color = fragment.context?.toColor(colorResource) ?: Color.WHITE
        toolbar?.setBackgroundColor(color)
    }

    fun setToolbarTitleColor(@ColorRes colorResource: Int) {
        val color = fragment.context?.toColor(colorResource) ?: Color.BLACK
        toolbar?.setTitleTextColor(color)
    }

    fun setToolbarNavigationIcon(@DrawableRes iconResource: Int) {
        toolbar?.setNavigationIcon(iconResource)
    }

    fun setToolbarTitle(title: String) {
        toolbar?.title = title
        toolbar?.subtitle = null
    }

    fun inflateMenu(@MenuRes menuRes: Int): Menu? {
        toolbar?.inflateMenu(menuRes)
        return toolbar?.menu
    }

    fun getMenu(): Menu? = toolbar?.menu

    fun clearMenu() = toolbar?.menu?.clear()
}

interface ToolbarNavigateUpListener {
    fun onNavigateUp(): Boolean
}