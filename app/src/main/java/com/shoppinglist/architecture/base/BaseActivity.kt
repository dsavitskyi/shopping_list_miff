package com.shoppinglist.architecture.base

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.shoppinglist.architecture.extensions.toast
import com.shoppinglist.architecture.internal.events.EventObserver
import com.shoppinglist.architecture.internal.makeStatusBarTransparent

abstract class BaseActivity<V : BaseViewModel<E>, E>: AppCompatActivity() {
    protected abstract val viewModel: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        makeStatusBarTransparent()
        subscribeToEvents()
        subscribeToToast()
    }

    private fun subscribeToEvents() {
        viewModel.eventsLiveData.observe(this, EventObserver {
            it?.let { onEvent(it) }
        })
        viewModel.logout.observe(this, Observer {
            if (it == true) {
                logout()
            }
        })
    }

    open fun logout() {

    }

    protected open fun onEvent(event: E) {

    }

    private fun subscribeToToast() {
        observe(viewModel.toastMessage) {
            when (it) {
                is String -> toast(it, Toast.LENGTH_SHORT)
                is Int -> toast(it, Toast.LENGTH_SHORT)
            }
        }
    }

    open fun showLoading(show: Boolean) {

    }

    fun <T, LD : LiveData<T>> observeNullable(liveData: LD, onChanged: (T?) -> Unit) {
        liveData.observe(this, Observer { value ->
            onChanged(value)
        })
    }

    fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer { value ->
            value?.let(onChanged)
        })
    }
}