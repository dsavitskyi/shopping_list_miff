package com.shoppinglist.utils

object Const {
    const val SPLASH_SCREEN_DELAY = 2000L
    const val FIREBASE_DB_URL = "https://shoppingapp-5d6de.firebaseio.com/"
    const val TABLE_NAME = "products"
}