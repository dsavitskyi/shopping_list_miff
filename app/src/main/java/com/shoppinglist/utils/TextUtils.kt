package com.shoppinglist.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object TextUtils {

    fun isValidEmail(email: String?): Boolean {
        return !email.isNullOrBlank() && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun formatDate(): String {
        val currentDate = Calendar.getInstance().time
        val df = SimpleDateFormat("dd MM yyyy HH:mm", Locale.getDefault())
        return df.format(currentDate)
    }
}